|Date |Title |Description |Link to MR|
|-----|------|-----------|-----------|
|14.10.2021|Task&nbsp;1|Создать файл readme.md и заполнить его информацией о себе: ФИО, специальность, курс, увлечения, хобби и т.д. Создать файл changelog.md и внести в него запись о сделанных изменениях в формате таблицы с колонками: Дата, название, описание, ссылка на MR|[MR 1](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/1)|
|19.10.2021|Task&nbsp;2|Макет № 3, в качестве фона использован только цвет, в галерее нет наложения изображений|[MR 2](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/2)|
|23.10.2021|Task&nbsp;4|Задания к лекции по теме JavaScript Overview|[MR 3](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/3)|
|11.11.2021|Task&nbsp;6|Домашнее задание к лекции на тему Javascript and Browser|[MR 4](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/4)|
|22.11.2021|Task&nbsp;5|Домашнее задание к лекции на тему Javascript Functions|[MR 5](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/5)|
|04.12.2021|Task&nbsp;7|Домашнее задание к лекции на тему Javascript and DOM|[MR 6](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/6)|
|29.12.2021|Task&nbsp;8|Gulp and Webpack|[MR 7](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/7)|
|30.12.2021|Task&nbsp:9|Домашнее задание к лекции на тему TypeScript|[MR 8](https://gitlab.com/fe_nc_vonorezh_2021/fs_irina_rusinova/-/merge_requests/8)|
