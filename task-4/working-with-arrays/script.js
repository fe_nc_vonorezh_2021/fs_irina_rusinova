function createArray(arraySize) {
    let arr = [];
    let i = 0;
    while (i < arraySize) {
        arr.push(Math.floor(Math.random() * 100));
        ++i;
    }
    return arr;
}

function sortArray(arr, direction) {
    let newArray;
   switch(direction) {
       case 'asc': {
           newArray = arr.slice(0);
           newArray.sort((a, b) => a - b);
           break;
       }
       case 'desc': {
           newArray = arr.slice(0);
           newArray.sort((a, b) => b - a);
           break;
       }
   }
    console.log('отсортированный массив:', newArray);
}

function sumSquaresOddElements(arr) {
    let sum = arr.reduce((sum, elem) => sum + ((elem % 2) ? elem * elem : 0), 0);
    console.log('сумма квадратов нечетных элементов:', sum);
}

const size = 5;
let arr = createArray(size);
sortArray(arr, 'asc');
sortArray(arr, 'desc');
sumSquaresOddElements(arr);