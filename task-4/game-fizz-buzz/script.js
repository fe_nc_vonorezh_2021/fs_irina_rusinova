function isMultipleOfThree(num) {
    const digits = String(num).split('');
    const sum = digits.reduce((sum, current) => sum + current, 0);
    return (sum % 3 == 0);
}

function isMultipleOfFive(num) {
    const lastDigit = num % 10;
    return (lastDigit == 0 || lastDigit == 5);
}

function show() {
    for (let i = 1; i <= 100; ++i) {
        const elem = isMultipleOfThree(i) ? (isMultipleOfFive(i) ? 'FizzBuzz' : 'Fizz') : (isMultipleOfFive(i) ? 'Buzz' : i);
        console.log(elem);
    }
}
show();