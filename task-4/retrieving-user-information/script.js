let name = prompt('Введите свое имя');
while (!name) {
    name = prompt('Введите корректное имя');
}

let age = prompt('Введите свой возраст');
while (age <= 0 || isNaN(parseInt(age))) {
    age = parseInt(prompt('Введите корректный возраст'));
}

alert(`Привет, ${name[0].toUpperCase() + name.slice(1)}, тебе уже ${age} лет!`);