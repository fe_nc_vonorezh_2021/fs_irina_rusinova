function swapNumbers(a, b) {
    let tmp = a;
    console.log('Before:\n', 'a = ', a, ', b = ', b);
    a = b;
    b = tmp;
    console.log('After:\n', 'a = ', a, ', b = ', b);
}

function swapNumbersWithoutAdditionalVariable(a, b) {
    console.log('Before:\n', 'a = ', a, ', b = ', b);
    a += b;
    b = a - b;
    a = a - b;
    console.log('After:\n', 'a = ', a, ', b = ', b);
}

let a = parseInt(prompt('Введите a'));
while (isNaN(a)) {
    a = parseInt(prompt('Введите a'));
}

let b = parseInt(prompt('Введите b'));
while (isNaN(b)) {
    b = parseInt(prompt('Введите b'));
}

swapNumbers(a, b);
swapNumbersWithoutAdditionalVariable(a, b);