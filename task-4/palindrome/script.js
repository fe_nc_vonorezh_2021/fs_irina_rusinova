function isWord(w) {
    for (key of w) {
        if (!key.match(/\D/)) {
            return false;
        }
    }
    return true;
}

function isPalindrome() {
    let word;
    while (!word || !isWord(word)) {
        word = prompt('Введите слово', '');
    }
    const length = word.length;
    for (let i = 0; i < length / 2; ++i) {
        if (word[i] !== word[length - 1 - i]) {
            alert(`${word} не палиндром!`);
            return false;
        }
    }
    alert(`${word} палиндром!`);
    return true;
}
isPalindrome();