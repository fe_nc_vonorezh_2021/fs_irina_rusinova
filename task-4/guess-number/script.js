function play() {
    const randomNumber = Math.floor(Math.random() * 1000 + 1);
    let count = 0;

    const getNumber = function() {
        let num = prompt('Введите число');
        ++count;
        while (!num || isNaN(parseInt(num))) {
            num = prompt('Введите число');
            ++count;
        }
        return num;
    }

    let num = getNumber();
    while (num != randomNumber) {
        if (num < randomNumber) {
            alert('Искомое число больше!');
            num = getNumber();
        } else {
            alert('Искомое число меньше!');
            num = getNumber();
        }
    } 
    let continuation = confirm(`Вы угадали! Количество попыток: ${count}. Начать заново?`);
    if (continuation) play();
}

button.onclick = play;