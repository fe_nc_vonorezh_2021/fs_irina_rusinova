const form = document.forms[0];

const regexp = {
    firstName: /\w+/,
    lastName: /\w+/,
    email: /^[\w.-]+@([\w-]+\.)+\w{2,}$/,
    phone: /^[\w.-]+@([\w-]+\.)+\w{2,}$|/,
    message: /\w+/
 };

const formFields = {
    firstName: form.elements['first-name'],
    lastName: form.elements['last-name'],
    email: form.elements['email'],
    phone: form.elements['phone'],
    message: form.elements['message']
}

const formButton = document.querySelector('.form-button');

let appealSent = false;

function getUserData(formFields) {
    const user = Object.entries(formFields).reduce((obj, current) => {
        obj[current[0]] = current[1].value;
        return obj;
    }, {});
    return user;
}

function getIncorrectlyFilledFields(user) {
    const incorrectlyFields = [];

    Object.entries(user).forEach(([key, value]) => {
        if (!regexp[key].test(value)) {
            switch(key) {
                case 'firstName': {
                    incorrectlyFields.push('имя');
                    break;
                }
                case 'lastName': {
                    incorrectlyFields.push('фамилия');
                    break;
                }
                case 'email': {
                    incorrectlyFields.push('email');
                    break;
                }
                case 'phone': {
                    incorrectlyFields.push('номер телефона');
                    break;
                }
                case 'message': {
                    incorrectlyFields.push('сообщение');
                    break;
                }
            }
        }
    });
    return incorrectlyFields;
}

function checkCorrectness() {
    const user = getUserData(formFields);
    const incorrectlyFields = getIncorrectlyFilledFields(user);

    if (incorrectlyFields.length) {
        alert(`"Поля ${incorrectlyFields.join(', ')} заполнены не верно, пожалуйста исправьте"`);
    } else {
        checkCookies(user);
    }
}

formButton.onclick = function() {
    checkCorrectness();
};


function setLocalStorage(formFields) {
    Object.entries(formFields).forEach(([key, field]) => {
        localStorage.setItem(key, field.value);
    });
}
window.addEventListener('beforeunload', setLocalStorage);

function getLocalStorage(formFields) {
    Object.entries(formFields).forEach(([key, field]) => {
        if (localStorage.getItem(key)) {
            field.value = localStorage.getItem(key);
        }
    });
}
window.addEventListener('load', getLocalStorage);


function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value) {
    let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value) + '; max-age=3600';
    document.cookie = updatedCookie;
}
  
function checkCookies(user) {
    if (appealSent) {
        alert(`${user.firstName} ${user.lastName}, ваше обращение обрабатывается!`);
    }
    else {
        setCookie('first-name', user.firstName);
        setCookie('last-name', user.lastName);
        alert(`${user.firstName} ${user.lastName}, спасибо за обращение!`);
        appealSent = true;
    }
}
