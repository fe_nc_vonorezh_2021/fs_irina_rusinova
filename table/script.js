const table = document.querySelector('table');

const addButton = document.querySelector('.add-button');
const editButton = document.querySelector('.edit-button');
const deleteButton = document.querySelector('.delete-button');
const cancelButton = document.querySelector('.cancel-button');

const textarea = document.querySelector('textarea');

cancelButton.onclick = function() {
    uncheck();
    showOneButton();
}

table.onclick = function(event) {
    uncheck();

    const currentCell = event.target;
    currentCell.classList.add('selected');
    showThreeButtons();

    editButton.onclick = function() {
        textarea.value = currentCell.textContent;
        textarea.classList.remove('hide');
        textarea.focus();

        textarea.onblur = function() {
            currentCell.textContent = textarea.value;
            textarea.classList.add('hide');
            currentCell.classList.remove('selected');
            showOneButton();
        }
    }
    
    deleteButton.onclick = function() {
        currentCell.remove();

        showOneButton();
    }
}


addButton.onclick = function() {
    const positionNewCell = [askPositionNewCell('row'), askPositionNewCell('column')];
    const rowsList = document.querySelectorAll('tr');
    
    let currentRow;
    let columnsList;
    let newCell;

    if (rowsList[positionNewCell[0]]) {
        currentRow = rowsList[positionNewCell[0]];
    } else {
        currentRow = document.createElement('tr');
        rowsList[rowsList.length - 1].after(currentRow); 
    }

    if (positionNewCell[0] == 0) {
        columnsList = currentRow.querySelectorAll('th');
        newCell = document.createElement('th');
        newCell.className = 'title';
    } else {
        columnsList = currentRow.querySelectorAll('td');
        newCell = document.createElement('td');
        newCell.className = 'td';
    }

   if (positionNewCell[1] < columnsList.length) {
       columnsList[positionNewCell[1]].before(newCell);
   } else {
       currentRow.append(newCell);
   }
}


function askPositionNewCell(rowOrColumn) {
    let rowOrColumnNumber;
    while (/\D/.test(rowOrColumnNumber)) {
        rowOrColumnNumber = prompt(`Enter ${rowOrColumn} number`, '');
    }
    return rowOrColumnNumber;
}


function showThreeButtons() {
    editButton.classList.remove('hide');
    deleteButton.classList.remove('hide');
    cancelButton.classList.remove('hide');
    addButton.classList.add('hide');
}

function showOneButton() {
    addButton.classList.remove('hide');
    editButton.classList.add('hide');
    deleteButton.classList.add('hide');
    cancelButton.classList.add('hide');
}

function uncheck() {
    const thList = table.querySelectorAll('.title');
    const tdList = table.querySelectorAll('.td');
    thList.forEach((cell) => cell.classList.remove('selected'));
    tdList.forEach((cell) => cell.classList.remove('selected'));
}
