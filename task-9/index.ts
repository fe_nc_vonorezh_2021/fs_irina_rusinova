import { RailwayCarriage } from "./railwaycarriage";
import { Train } from "./train";
import { Timetable } from "./timetable";
import { Passenger, IPassenger, IPassengerTrain } from "./passenger";

const car1: RailwayCarriage = new RailwayCarriage(1, 'freight');
car1.showInformation();

const train11: Train = new Train(11, 1, 'passenger');
train11.showInformation();


const train21: Train = new Train(21, 4, 'passenger');
const timetable = new Timetable();
train11.addDirection('A-B');
timetable.addTrain('10.00', train11);
train21.addDirection('A-C');
timetable.addTrain('10.30', train21);
timetable.addTrain('10.00', new Train(31, 1, 'freight'));
timetable.show();


const p: Passenger = new Passenger('Name', 1, 1);
p.show();

const p2: IPassengerTrain = {
    name: 'First name',
    train: 1,
    carriage: 1
};
