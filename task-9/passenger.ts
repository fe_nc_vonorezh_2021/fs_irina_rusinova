import { Train } from "./train";

export class Passenger implements IPassenger {
    public name: string;
    public train: number;
    public carriage: number;

    constructor(name: string, train: number, carriage: number) {
        this.name = name;
        this.train = train;
        this.carriage = carriage;
    }

    public show(): void {
        console.log(`${this.name} is in train number ${this.train} carriage number ${this.carriage}`);
        
        const train = new Train(this.train, this.carriage, 'passenger');
        train.showInformation();
    }
}

export interface IPassenger {
    name: string;
}

export interface IPassengerTrain extends IPassenger {
    train: number;
    carriage: number;
}
