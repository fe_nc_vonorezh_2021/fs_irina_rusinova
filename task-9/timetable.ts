import { Train } from "./train";

export class Timetable {
    public passengerTrains: string[];
    public freightTrains: string[];

    constructor() {
        this.passengerTrains = [];
        this.freightTrains = [];
    }

    public addTrain(time: string, train: Train): void {
        switch(train.type) {
            case 'freight': {
                this.freightTrains.push(`${time}: train ${train.train} (${train.direction || ''})`);
                break;
            }
            case 'passenger': {
                this.passengerTrains.push(`${time}: train ${train.train} (${train.direction || ''})`);
                break;
            }
        }
    }

    public show(): void {
        console.log('\nPassenger trains:');
        this.passengerTrains.map(line => console.log(line));
        console.log('\nFreight trains:');
        this.freightTrains.map(line => console.log(line));
    }
}
