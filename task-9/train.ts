import { RailwayCarriage } from "./railwaycarriage";

export class Train extends RailwayCarriage {
    public carsNumberList: RailwayCarriage[];
    public direction: string;
    private trainNumber: number;

    constructor(trainNumber: number, currentCarriage: number, type: string) {
        super(currentCarriage, type);

        this.trainNumber = trainNumber;
    }

    get train() {
        return this.trainNumber;
    }

    set train(number: number) {
        this.trainNumber = number;
    }

    public showInformation(): void {
        console.log('About the carriage:');
        super.showInformation();

        console.log('About the train:');
        console.log(`this ${this.type} train number ${this.trainNumber} has carriage ${this.carriage}`);
    }

    public addDirection(direction: string): void {
        this.direction = direction;
    }
}
