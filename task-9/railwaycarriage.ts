export class RailwayCarriage {
    private carNumber: number;
    private carType: string;

    constructor(carNumber: number, type: string) {
        this.carNumber = carNumber;
        this.carType = type;
    }

    get carriage(): number {
        return this.carNumber;
    }

    set carriage(number: number) {
        this.carNumber = number;
    }

    get type(): string {
        return this.carType;
    }

    set type(type: string) {
        this.carType = type;
    }

    public showInformation(): void {
        console.log(`this ${this.type} railwayCarriage has number ${this.carriage}`);
    }
}
