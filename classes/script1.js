class RailwayCarriage {
    constructor(carNumber, type) {
        this._carNumber = carNumber;
        this._type = type;
    }

    get carNumber() {
        return this._carNumber;
    }

    set carNumber(number) {
        this._carNumber = number;
    }

    get type() {
        return this._type;
    }

    set type(type) {
        this._type = type;
    }

    showInformation() {
        console.log(`this ${this.type} railwayCarriage has number ${this.carNumber}`);
    }
}

class Train extends RailwayCarriage {
    constructor(trainNumber, carriageNumbers, type) {
        const list = [super(carriageNumbers[0], type)];
        for (let i = 1; i < carriageNumbers.length; ++i) {
            list.push(new RailwayCarriage(carriageNumbers[i], type));
        }
        this.carsNumberList = list;
        this._trainNumber = trainNumber;
    }

    get trainNumber() {
        return this._trainNumber;
    }

    set trainNumber(number) {
        this._trainNumber = number;
    }

    showInformation() {
        const carsNumberList = this.carsNumberList.reduce((list, car) => {
            list.push(car.carNumber);
            return list;
        }, []).join(', ');

        console.log('About the first carriage:');
        super.showInformation();

        console.log('About the train:');
        console.log(`this ${this.type} train number ${this.trainNumber} has carriage ${carsNumberList}`);
    }

    addDirection(direction) {
        this.direction = direction;
    }
}

const car1 = new RailwayCarriage(1, 'freight');
car1.showInformation();

const train11 = new Train(11, [1, 2, 5, 4], 'passenger');
train11.showInformation();


class Timetable {
    constructor() {
        this.passengerTrains = [];
        this.freightTrains = [];
    }

    addTrain(time, train) {
        switch(train.type) {
            case 'freight': {
                this.freightTrains.push(`${time}: train ${train.trainNumber} (${train.direction || ''})`);
                break;
            }
            case 'passenger': {
                this.passengerTrains.push(`${time}: train ${train.trainNumber} (${train.direction || ''})`);
                break;
            }
        }
    }

    show() {
        console.log('\nPassenger trains:');
        this.passengerTrains.map(line => console.log(line));
        console.log('\nFreight trains:');
        this.freightTrains.map(line => console.log(line));
    }
}


const train21 = new Train(21, [1, 2, 3, 4, 5, 6], 'passenger');
const timetable = new Timetable();
train11.addDirection('A-B');
timetable.addTrain('10.00', train11);
train21.addDirection('A-C');
timetable.addTrain('10.30', train21);
timetable.addTrain('10.00', new Train(31, [1], 'freight'));
timetable.show();


class Passenger {
    constructor(name, {train, carriage}) {
        this.name = name;
        this.train = train;
        this.carriage = carriage;
    }

    show() {
        console.log(`${this.name} is in train number ${this.train} carriage number ${this.carriage}`);
        const train = new Train(this.train, [1, this.carriage], 'passenger');
        train.showInformation();
    }
}

const p = new Passenger('Name', {train: 1, carriage: 1});
p.show();
