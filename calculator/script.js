const output = document.querySelector('.output');


function reset() {
    if (output.textContent.length > 1) {
        const outputTextContent = output.textContent;
        output.textContent = outputTextContent.slice(0, -1);
    } else {
        output.textContent = '0';
    }
}

function addZero() {
    if (output.textContent !== '0') {
        output.textContent += '0';
    }
}

function addDigit(digit) {
    output.textContent = output.textContent !== '0' ? output.textContent + digit : digit;
}

function addComma() {
    isNaN(parseInt(output.textContent.slice(-1), 10)) ? output.textContent += '0.' : output.textContent += '.';
}

const cancel = () => output.textContent = '0';

function addOperator(operator) {
    output.textContent += operator;
}

function getRoot() {
    isNaN(parseInt(output.textContent.slice(-1))) ? output.textContent += 'sqrt' : output.textContent += '+sqrt';
}

function multiplyByMinusOne() {
    const index = Math.max(output.textContent.lastIndexOf('+'), output.textContent.lastIndexOf('-'));

    if (index === -1) {
        output.textContent = '-' + output.textContent;
    } else {
        let outputTextContent = output.textContent.split('');
        if (output.textContent[index] === '+') {
            outputTextContent.splice(index, 1, '-').join('');
        }
        else {
            outputTextContent.splice(index, 1, '+').join('');
        }
        output.textContent = outputTextContent.join('');
    }
}

const solve = function() {
    const result = solveInOrder(convertToArray(output.textContent));
    output.textContent = result;
}


function convertToArray(str) {
    let number = [];
    let expression = [];
    for (let i = 0; i < str.length; ++i) {
        if (str[i] === 's') {
            expression.push(str.slice(i, i + 4));
            i += 4;
        }

        if (/[0-9.]/.test(str[i])) {
            number.push(str[i]);
        } else {
            expression.push(number.join(''));
            number = [];
            if (str[i] === '-') {
                expression.push('+', -1, '*');
            } else {
                expression.push(str[i]);
            }
        }
    }
    expression.push(number.join(''));
    return expression;
}

function solveInOrder(arr) {
    for (let i = 0; i < arr.length; ++i) {
        if (arr[i] === 'sqrt') {
            arr.splice(i, 2, Math.sqrt(arr[i + 1]));
            --i;
        }
    }
    for (let i = 0; i < arr.length; ++i) {
        if (arr[i] === '**' || arr[i] == '^') {
            arr.splice(i - 1, 3, arr[i - 1] ** arr[i + 1]);
            i -=2;
        }
    }
    for (let i = 0; i < arr.length; ++i) {
        if (arr[i] === '/') {
            arr.splice(i - 1, 3, arr[i - 1] / arr[i + 1]);
            i -= 2;
        }
    }
    for (let i = 0; i < arr.length; ++i) {
        if (arr[i] === '*') {
            arr.splice(i - 1, 3, arr[i - 1] * arr[i + 1]);
            i -= 2;
        }
    }
    for (let i = 0; i < arr.length; ++i) {
        if (arr[i] === '+') {
            arr.splice(i - 1, 3, +arr[i - 1] + +arr[i + 1]);
            i -= 2;
        }
    }
    for (let i = 0; i < arr.length; ++i) {
        if (arr[i] === '-') {
            arr.splice(i - 1, 3, arr[i - 1] - arr[i + 1]);
            i -= 2;
        }
    }
    return arr[0];
}
